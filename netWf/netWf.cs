﻿using log4net;
using Nancy;
using Nancy.Conventions;
using Nancy.Hosting.Self;
using Nancy.Session;
using System;
using System.IO;
using System.Net;

// netWf 
//
// netWf netDb Web Framework
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf
{
    /// <summary>
    /// netDb Web Framework
    /// </summary>
	public class netWf
	{
        /// <summary>
        /// Listen Port
        /// </summary>
        private int _port = 0;

        /// <summary>
        /// Logger Object
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(netWf));

        /// <summary>
        /// Nancy Web Host
        /// </summary>
        private NancyHost host = null;

        /// <summary>
        /// Current Directory
        /// </summary>
        private string _currentDir = "";

        /// <summary>
        /// Indicates if Service is Started
        /// </summary>
        private bool _started = false;

        /// <summary>
        /// Create New Instance
        /// </summary>
        /// <param name="port">
        /// Listen Port
        /// </param>
        /// <param name="key">
        /// Default Master Key
        /// </param>
        /// <param name="tableLogin">
        /// Table Login
        /// </param>
		public netWf (int port, string key, string tableLogin)
		{
            _port = port;
 
            // Check Database Connection
            netDb.netDb db = netDb.netDb.GetInstance();

            if (! db.IsConnected())
            {
                logger.Fatal("Could not connect to Database");

                Console.WriteLine("Could not connect to Database");

                System.Environment.Exit(1);
            }
                

            // Add Default Key
            KeyManager.AddDefaultKey(key, 30);

            // Current Directory
            _currentDir = Environment.CurrentDirectory.Replace("\\", "/");

            // Init TableLogin
            TableLogin.getInstance(tableLogin);
		}

        /// <summary>
        /// Start Web Services
        /// </summary>
		public void start()
		{
            string url = "http://localhost:" + _port;

            // If is Windows requires Administrator Permissions
            if (System.Environment.OSVersion.ToString().Contains("Windows"))
            {
                HostConfiguration config = new HostConfiguration();
                config.UrlReservations.CreateAutomatically = true;
                config.UrlReservations.User = "Everyone";

                host = new NancyHost(config, new Uri(url));
            }
            else
            {
                host = new NancyHost(new Uri(url));
            }

            // Create Index
            createDir();
            createIndex();

            // Download js Libraries
            downloadJs();

            // Start Web Service
            host.Start();

            _started = true;

            logger.Info("Listening on: " + url);
		}

        /// <summary>
        /// Stop Web Services
        /// </summary>
        public void stop()
        {
            host.Stop();
            
            _started = false;

            logger.Info("Stoped Application");
        }

        /// <summary>
        /// Return true if service is started else false
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public bool isStarted()
        {
            return _started;
        }

        /// <summary>
        /// Creates Web Directory if not exists
        /// </summary>
        private void createDir()
        {
            // Create Web Directory
            string webDir = _currentDir + "/static/web";

            if (! Directory.Exists(webDir))
                Directory.CreateDirectory(webDir);

            // Create js Directory
            webDir = _currentDir + "/static/web/js";

            if (! Directory.Exists(webDir))
                Directory.CreateDirectory(webDir);

            // Create Scripts Directory
            webDir = _currentDir + "/scripts";

            if (!Directory.Exists(webDir))
                Directory.CreateDirectory(webDir);

            webDir = null;
        }

        /// <summary>
        /// Create index.html if not exists
        /// </summary>
        private void createIndex()
        {
            string indexContent = "<html><body><h2>Welcome to netDb Web Framework !!!</h2></body></html>";

            string webDir = _currentDir + "/static/web";

            string indexFile = webDir + "/index.html";

            if (! File.Exists(indexFile))
                File.WriteAllText(indexFile, indexContent);
        }

        /// <summary>
        /// Download js Libraries
        /// </summary>
        private void downloadJs()
        {
            WebClient web = new WebClient();
            string webDir = _currentDir + "/static/web/js";

            if (! File.Exists(webDir + "/netWf.js"))
                web.DownloadFile("http://cdn.3utilities.com/js/netwf/netWf.js", webDir + "/netWf.js");
        }
	}

    /// <summary>
    /// Override Class to Custom Configurations
    /// </summary>
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        /// <summary>
        /// Overload Content Conventions
        /// </summary>
        /// <param name="nancyConventions">
        /// nancyConventions
        /// </param>
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            nancyConventions.StaticContentsConventions.Clear();

            nancyConventions.StaticContentsConventions.Add
                (StaticContentConventionBuilder.AddDirectory("web", "/static/web"));
        }

        /// <summary>
        /// Enable Sessions
        /// </summary>
        /// <param name="container">
        /// container
        /// </param>
        /// <param name="pipelines">
        /// pipelines
        /// </param>
        protected override void ApplicationStartup(Nancy.TinyIoc.TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            CookieBasedSessions.Enable(pipelines);
        }
    }
}

