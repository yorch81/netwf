﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Login 
//
// Login Abstract Login Class
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf
{
    /// <summary>
    /// Abstract Login Class
    /// </summary>
    public abstract class Login
    {
        /// <summary>
        /// Check User and Password Login
        /// </summary>
        /// <param name="user">
        /// Login User
        /// </param>
        /// <param name="password">
        /// Password of User
        /// </param>
        /// <returns>
        /// bool
        /// </returns>
        public abstract bool check(string user, string password);
    }
}
