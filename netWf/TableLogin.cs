﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// TableLogin 
//
// TableLogin Table Login Implementation
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf
{
    /// <summary>
    /// Table Login
    /// </summary>
    class TableLogin : Login
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static TableLogin INSTANCE = null;

        /// <summary>
        /// Table Login
        /// </summary>
        private string _tableLogin = "";

        /// <summary>
        /// Logger Object.
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(TableLogin));

        /// <summary>
        /// Private Constructor
        /// </summary>
        /// <param name="tableLogin">
        /// Table Login
        /// </param>
        private TableLogin(string tableLogin)
        {
            _tableLogin = tableLogin;
        }

        /// <summary>
        /// Gets Singleton Instance
        /// </summary>
        /// <param name="tableLogin">
        /// Table Login
        /// </param>
        /// <returns>
        /// TableLogin
        /// </returns>
        public static TableLogin getInstance(string tableLogin = "")
        {
            if (INSTANCE == null)
                INSTANCE = new TableLogin(tableLogin);

            return INSTANCE;
        }

        /// <summary>
        /// Validates User and Password
        /// </summary>
        /// <param name="user">
        /// Login User
        /// </param>
        /// <param name="password">
        /// Password User
        /// </param>
        /// <returns>
        /// bool
        /// </returns>
        public override bool check(string user, string password)
        {
            bool retValue = false;

            netDb.netDb db = netDb.netDb.GetInstance();

            if (db.IsConnected())
            {
                string query = "SELECT * FROM " + _tableLogin + " WHERE nuser = @user AND npassword = @pwd";
                DataSet ds = db.Execute(query, db.GetParameter("@user", user), db.GetParameter("@pwd", password));
                
                try
                {
                    if (ds.Tables[0].Rows[0]["nuser"].Equals(user))
                        retValue = true;
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                    retValue = false;
                }
            }
            
            return retValue;
        }
    }
}
