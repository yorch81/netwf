# netDb Web Framework #

## Description ##
netDb Web Framework.

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [NancyFx](http://nancyfx.org/)
* [netDb](https://bitbucket.org/yorch81/netdb)
* [SQL Server](http://www.microsoft.com/es-es/server-cloud/products/sql-server/)
* [MySQL](http://www.mysql.com/)
* [netWf.js](http://cdn.3utilities.com/js/netwf/netWf.js)
* [Web Application Example](https://bitbucket.org/yorch81/netsrv)

## Developer Documentation ##
In the Code.

## Installation ##
Add nuget reference: PM> Install-Package netWf.dll

## Notes ##
For install dependencies, please run: Update-package -reinstall

The library creates a /static/web/ directory as home of webserver and download the js library netWf.js, 
with this JavaScript library you can access to the webservices, and creates /scripts for publish sql scripts.

If the Operating System is Windows must run as Administrator.

SQL Server Login Table Example:
~~~

CREATE TABLE [dbo].[netusers](
	[id] [numeric](11, 0) IDENTITY(1,1) NOT NULL,
	[nuser] [varchar](60) NOT NULL,
	[npassword] [varchar](80) NOT NULL
) ON [PRIMARY]


INSERT INTO [dbo].[netusers]
           ([nuser]
           ,[npassword])
VALUES('myuser'
     ,(SELECT LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', 'mypassword'), 2))))

~~~

MySQL Login Table Example:
~~~

CREATE TABLE `netusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nuser` varchar(60) NOT NULL,
  `npassword` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `markers`.`netusers`
(`id`,
`nuser`,
`npassword`)
VALUES
(0,
'myuser',
md5('mypassword'));

~~~

## Example ##
~~~

// Init Database Connection
netDb.netDb.GetInstance(netDb.netDb.MSSQLSERVER, serverDb, userDb, password, dbName, 1433);

// Init Web Service
netWf.netWf web = new netWf.netWf(port, key, tableUsers);

web.start();

Console.WriteLine("Listening on " + port + " ...");
Console.WriteLine("Enter CTRL + C or 'quit' to Terminate Web Application");

string command = "";

while (command != "quit")
    command = Console.ReadLine();

web.stop();

~~~

## REST Api ##
Rows number by Table:
$ curl http://MYURL:PORT/api/table/TABLE_NAME/ROWS?auth=MASTER_KEY

One row by Id:
$ curl http://MYURL:PORT/api/tablebyid/TABLE_NAME/FIELD/KEY?auth=MASTER_KEY

First row:
$ curl http://MYURL:PORT/api/first/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Next row:
$ curl http://MYURL:PORT/api/next/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Previous row:
$ curl http://MYURL:PORT/api/previous/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Last row:
$ curl http://MYURL:PORT/api/last/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Execute Stored Procedure:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL:PORT/api/procedure/PROCEDURE_NAME?auth=MASTER_KEY

Execute SQL Script:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL:PORT/api/script/SCRIPT_NAME?auth=MASTER_KEY

## Add Custom Routes ##
To add custom routes, you can create a public class extending of NancyModule like the example, except the 
reserved routes /, /user, /key, /api*.

~~~

public class CustomRoutes : NancyModule
{
    public CustomRoutes()
    {
        Get["/custom"] = parameters =>
        {
            Response response = new Response();

            response = (Response)"Custom Route";
            response.StatusCode = HttpStatusCode.OK;

            return response;
        };
    }
}

~~~

## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer